package main

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
)

func main() {

	router := gin.Default()
	router.LoadHTMLGlob("templates/*")
	router.Static("/static", "./static")

	router.GET("/", index)

	err := router.Run(fmt.Sprintf("%s:%v", Config.Host.HostName, Config.Host.Port))
	if err != nil {
		panic(err)
	}

}

func index(c *gin.Context) {

	c.HTML(http.StatusOK, "index.html", nil)
}
