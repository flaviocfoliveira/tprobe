package main

import (
	"encoding/json"
	"flag"
	"io/ioutil"
)

var Config *Configurations

func init() {

	var configFile string
	flag.StringVar(&configFile, "config", "config.json", "the path do the config.json file")
	flag.Parse()

	if b, err := ioutil.ReadFile(configFile); err != nil {
		panic(err)
	} else {

		var cfg Configurations
		if err := json.Unmarshal(b, &cfg); err != nil {
			panic(err)
		} else {

			Config = &cfg

		}

	}

}
