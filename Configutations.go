package main

type Configurations struct {
	Host *HostSettings `json:"Host"`
}
type HostSettings struct {
	HostName    string `json:"HostName"`
	Port        int    `json:"Port"`
	BaseURL     string `json:"BaseURL"`
	WebsiteName string `json:"WebsiteName"`
}
